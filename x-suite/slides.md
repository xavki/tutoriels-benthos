%title: Benthos
%author: xavki


# BENTHOS : Installation & Hello World !!!


<br>

nom;prenom;ville;age
durand;pierre;caen;43
dupond;julie;paris;26



input:
  file:
    paths: [ ./data/*.csv ]
    codec: "csv:;"
    max_buffer: 1
    delete_on_finish: false

pipeline:
  processors:
    - sleep:
        duration: 5s
    - mapping: |
        root = this
        root.path = meta("path")
    - group_by:
      - check: this.nom.contains("dupond")
        processors:
          - mapping: 'meta grouping = "dupond"'
output:
  switch:
    cases:
      - check: meta("grouping") == "dupond"
        output:
          file:
            path: "./data/output/dupond.txt"
            codec: lines
      - output:
          file:
            path: "./data/output/others.txt"
            codec: lines

