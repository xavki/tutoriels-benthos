%title: Benthos
%author: xavki


# BENTHOS : Installation & Hello World !!!


<br>

* installation du binaire

```
curl -Lsf https://sh.benthos.dev | bash
```

<br>

* avec docker

```
docker run --rm -v $(pwd)/config.yaml:/benthos.yaml jeffail/benthos
```

---------------------------------------------------------

# BENTHOS : Installation & Hello World !!!

<br>

* générer une configuration (valeurs par défaut)

```
benthos create > config.yaml
```

---------------------------------------------------------

# BENTHOS : Installation & Hello World !!!

<br>

ou

```
input:
  stdin: {}
pipeline:
  processors: 
    - mapping: root = content().uppercase()
output:
  stdout: {}
```

---------------------------------------------------------

# BENTHOS : Installation & Hello World !!!

<br>

* pour lancer la configuration

```
benthos -c config.yaml
```

---------------------------------------------------------

# BENTHOS : Installation & Hello World !!!

<br>

* autre exemple

```
input:
  stdin: {}
pipeline:
  processors:
    - sleep:
        duration: 5s
    - mapping: |
        root.input = this.message
        root.majuscule = this.message.uppercase()
output:
  stdout: {}
```

---------------------------------------------------------

# BENTHOS : Installation & Hello World !!!

<br>

* bah... en plus ya déjà des métriques :)

```
curl http://192.168.12.60:4195/metrics
```
