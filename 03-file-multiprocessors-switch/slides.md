%title: Benthos
%author: xavki


# BENTHOS : Installation & Hello World !!!


<br>

* un fichier source format CSV

```
nom;prenom;ville;age
durand;pierre;caen;43
dupond;julie;paris;26
dupond;paul;bordeaux;26
```

-----------------------------------------------------------------

# BENTHOS : Installation & Hello World !!!

<br>

* input de type file

```
input:
  file:
    paths: [ ./data/*.csv ]
    codec: "csv:;"
    max_buffer: 1
    delete_on_finish: false
```

-----------------------------------------------------------------

# BENTHOS : Installation & Hello World !!!

<br>

* pipeline pour processer la donnée

		* processors : liste d'actions

		* threads (default -1) : nombre de CPU

```
pipeline:
  threads: 4
  processors:
```

Attention : les processors peuvent être utilisés comme output (reject)

-----------------------------------------------------------------

# BENTHOS : Installation & Hello World !!!

<br>

* processor - sleep

```
input:
  file:
    paths: [ ./data/*.csv ]
    codec: "csv:;"
    delete_on_finish: false
pipeline:
  processors:
    - sleep:
        duration: 5s
output:
  file:
    path: "./data/output/output.txt"
    codec: lines
```

-----------------------------------------------------------------

# BENTHOS : Installation & Hello World !!!

<br>

* multi processors

```
pipeline:
  processors:
    - sleep:
        duration: 5s
    - mapping: |
        root = this
        root.age = this.age
        root.nom = this.nom
        root.ville = this.ville
```

-----------------------------------------------------------------

# BENTHOS : Installation & Hello World !!!

<br>

* notion de groupe - group_by

* ajout d'un tag meta

```
pipeline:
  processors:
    - sleep:
        duration: 5s
    - mapping: |
        root = this
        root.path = meta("path")
    - group_by:
      - check: this.nom.contains("dupond")
        processors:
          - mapping: 'meta grouping = "dupond"'
```

-----------------------------------------------------------------

# BENTHOS : Installation & Hello World !!!

<br>

* récupération dans l'output

```
output:
  switch:
    cases:
      - check: meta("grouping") == "dupond"
        output:
          file:
            path: "./data/output/dupond.txt"
            codec: lines
      - output:
          file:
            path: "./data/output/others.txt"
            codec: lines
```
