%title: Benthos
%author: xavki


# BENTHOS : INTRODUCTION


<br>

* Streaming de data :

	* récupérer via des sources

	* processer, hydrater, transformer...

	* injection, stockage de la donnée transformée

--------------------------------------------------------------------------------

# BENTHOS : INTRODUCTION


<br>

* Outils similaires : filebeat, logstash, fluentbit, fluentd...

* Développé en Go

* Mode déclaratif en Yaml

* Garantie de transmission des messages

--------------------------------------------------------------------------------

# BENTHOS : INTRODUCTION

<br>

Site : https://www.benthos.dev
Github : https://github.com/benthosdev/benthos
Youtube : https://www.youtube.com/channel/UCjIYEhBrw3GQwpRWe1asufg

--------------------------------------------------------------------------------

# BENTHOS : INTRODUCTION

<br>

* sous forme :

		* binaire (oneline)

		* container

		* chart helm & kubernetes

--------------------------------------------------------------------------------

# BENTHOS : INTRODUCTION

<br>


* 3 groupes de composants

		* Inputs

		* Processors

		* Outputs

--------------------------------------------------------------------------------

# BENTHOS : INTRODUCTION

<br>

* possibilité de compléter l'exécution :

		* caches

		* logs

		* métriques

		* tracing

<br>

* dispose d'un langage interne bloblang
		* permet d'autres langages (awk)

--------------------------------------------------------------------------------

# BENTHOS : INTRODUCTION

<br>

* studio d'aide à l'édition de configuration

<br>

* interactions avec des services de cloud providers
